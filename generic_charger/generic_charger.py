import csv
from datetime import datetime
from exness.models import Balance
from exness.models_share import Logins

from exness.Secured.API.mt_billing_api import change_balance

from exness.Secured.Convert import CConvert


FILENAME = "trade_marathon_charges.csv"


def create_balance_record(converter, login, operation_type, comment, amount, currency):

    login_rec = Logins.objects.get(Login=login)

    params = {
        "Type": "Deposit",
        "Status": 1,
        "Server": login_rec.Server,
        "OperationType": operation_type,
        "Login": login,
        # "TransactionID": kwargs.id,

        "EPSCommission": 0,
        "BenAddress": comment or "service charge at {}".format(str(datetime.now()).split('.')[0]),

        "Amount": converter.Do(currency, login_rec.currency, amount),
        "UserCurrency": login_rec.currency,

        "InCurrencyAmount": amount,
        "OperationCurrency": currency,

        "CInCurrencyAmount": converter.Do(currency, login_rec.currency, amount,),
        "CAmount": converter.Do(currency, login_rec.currency, amount),
    }

    balance = Balance(**params)
    return balance


def process(filename):
    errors = []
    success = []
    charges = []

    with open(filename, "r") as charges_csv:
        charges_reader = csv.reader(charges_csv, delimiter=',')
        for num, charge in enumerate(charges_reader):
            charges.append({
                "row_number": num,
                "account": charge[0],
                "amount": charge[1],
                "currency": "USD",  # TODO: make it as parameter
                "operation_type": charge[2],
                "transaction_id": charge[3],
            })

    for charge in charges:
        print(charge)
