import csv
from exness.Secured.API.mt_billing_api import change_balance
from exness.models_share import Logins

from exness.Secured.Convert import CConvert
PATH = "psp_logins.csv"
DEPOSIT = "Deposit"
charges = []
report = []


class BalanceRecord(object):
    def __init__(self, **kwargs):
        self.id = kwargs['id']
        self.Login = kwargs['Login']
        self.Server = kwargs['Server']
        self.IsInt = kwargs['IsInt']
        self.Amount = kwargs['Amount']
        self.UserCurrency = kwargs['UserCurrency']
        self.OperationType = 'Other'
        self.Account = kwargs['Account']
        self.Type = kwargs['Type']
        self.CAmount = kwargs['Amount']


def process_file():
    with open(PATH, "r") as charges_csv:
        charges_reader = csv.reader(charges_csv, delimiter=',')
        for charge in charges_reader:
            charges.append(charge)

    Convert = CConvert()

    for server, login, currency, amount in charges[1:]:
        try:
            login = Logins.objects.get(Login=login)
            assert login.Server == server
        except Logins.DoesNotExist:
            report.append("{}   doesnt exist".format(charge))
            print(charge[0], "Doesnt exist")
            continue

        usd_amount = charge[1]
        amount = Convert.Do("USD", login.currency, usd_amount)

        balance_record = BalanceRecord(**{
            "id": 1,
            "Login": login.Login,
            "Server": login.Server,
            "IsInt": False,
            "Amount": amount,
            "UserCurrency": login.currency,
            "OperationType": "OTHER",
            "Account": login.Login,
            "Type": DEPOSIT,
            "CAmount": amount,
        })

        try:
            # order_id = change_balance(balance_record=balance_record)
            order_id = '666'
            print("OK", charge, (amount, login.currency), order_id)
        except Exception as e:
            print("FAIL", charge, e)

        report.append("{}\n".format(str(order_id)))

    with open("report", "w") as report_file:
        report_file.writelines(report)


if __name__ == "__main__":
    process_file()
