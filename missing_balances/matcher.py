import csv

class Balances:
    @staticmethod
    def parse_balances_report(account):
        filename = f"{account}_balances.csv"
        balances = {}

        with open(filename, newline='') as csvfile:
            balances_reader = csv.reader(csvfile, delimiter=',')
            for row in balances_reader:
                if row[0] == "id":
                    continue

                balances[row[0]] = {
                    "id": row[0],
                    "login": row[1],
                    "type": row[2],  # Withdrawal / Deposit
                    "OpType": row[3],
                    "Amount": row[4].split(" ")[0],
                    "Currency": row[4].split(" ")[1],
                    "Account": row[5],
                    "Date": row[6],
                    "L": row[7],
                    "Status": row[8],
                    "MT_Ticker": row[9],
                    "TrnID": row[10],
                    "Staff": row[11],
                }

        print(f"{len(balances)} balances found")
        return balances

    @staticmethod
    def balances_missing_mtticker(balances):
        result = {}
        for balance_id in balances:
            if (
                not balances[balance_id]["MT_Ticker"]
                and balances[balance_id]["Status"] == "ACCEPTED"
                and balances[balance_id]["OpType"] == "BONUS"
            ):
                result[balance_id] = balances[balance_id]

        print(f"{len(result)} ACCEPTED balances WITHOUT mt ticker: {list(result.keys())[0: 3]}...")
        return result

    @staticmethod
    def balances_with_mtticker(balances):
        result = {}
        for balance_id in balances:
            if (
                balances[balance_id]["MT_Ticker"]
                and balances[balance_id]["Status"] == "ACCEPTED"
                and balances[balance_id]["OpType"] == "BONUS"
            ):
                result[balance_id] = balances[balance_id]

        print(f"{len(result)} ACCEPTED balances WITH mt ticker: {list(result.keys())[0: 3]}...")
        return result

    @staticmethod
    def get_balances_ids_as_mt_comments(balances):
        result = []
        for balance in balances.values():
            # W-BONUS-USD-145057242
            result.append(
                f"{balance['type'][0]}-"
                f"{balance['OpType']}-"
                f"{balance['Currency']}-"
                f"{balance['id']}"
            )

        print(f"{len(set(result))} suspiciouts comment from balances")
        return set(result)

    @staticmethod
    def get_balances_ids_from_comments(comments):
        return [comment.split("-")[-1] for comment in comments]



class Orders:
    @staticmethod
    def parse_orders_report(account):
        filename = f"{account}_orders.csv"
        orders = {}

        with open(filename, newline='') as csvfile:
            balances_reader = csv.reader(csvfile, delimiter=',')
            for row in balances_reader:
                if row[0] == "open_time":
                    continue

                orders[f"{row[13]}"] = {
                    "open_time": row[0],
                    "close_time": row[1],
                    "order_type": row[2],
                    "symbol": row[3],
                    "volume": row[4],
                    "open_price": row[5],
                    "close_price": row[6],
                    "sl": row[7],
                    "tp": row[8],
                    "swap": row[9],
                    "commission": row[10],
                    "profit": row[11],
                    "comment": row[12],
                    "balance_id_from_comment": row[12].split("-")[-1],
                    "order_id": row[13],
                    "usd_volume": row[14],
                    "contract_size": row[15],
                    "avg_spread": row[16]
                }

        print(f"{len(orders)} orders found")
        return orders

    @staticmethod
    def get_balance_orders(orders):
        result = {}
        for order_id in orders:
            if (
                    orders[order_id]["order_type"] == "balance"
            ):
                result[order_id] = orders[order_id]

        print(f"{len(result)} BALANCE orders: {list(result.keys())[0: 3]}...")
        return result

    @staticmethod
    def get_balances_ids_from_orders(orders):
        result = set([order["balance_id_from_comment"] for order in orders.values()])
        print(f"{len(result)} uniqie balance_ids: {list(result)[0: 3]}")
        return result


if __name__ == "__main__":
    from sys import argv
    account = int(argv[2])

    print(f"Running recon for {account}\n")
    balances = Balances.parse_balances_report(account=account)
    mtless_balances = Balances.balances_missing_mtticker(balances=balances)
    mtfull_balances = Balances.balances_with_mtticker(balances=balances)

    all_balances_ids = set(balances.keys())

    mtless_balances_ids = set(mtless_balances.keys())
    mtfull_balances_ids = set(mtfull_balances.keys())

    print()

    orders = Orders.parse_orders_report(account=account)
    balance_orders = Orders.get_balance_orders(orders=orders)
    orders_balances_ids = Orders.get_balances_ids_from_orders(orders=balance_orders)

    print()
    print(
        f"presented in balances and mt "
        f"{len(mtfull_balances_ids.intersection(orders_balances_ids))}, "
        f"{list(mtfull_balances_ids.intersection(orders_balances_ids))[0: 3]}..."
    )

    print(
        f"presented only in balances "
        f"{len(mtless_balances_ids - orders_balances_ids)}, "
        f"{list(mtless_balances_ids - orders_balances_ids)[0: 3]}..."
    )
    print(
        f"presented only in orders "
        f"{len(orders_balances_ids - all_balances_ids)}, "
        f"{list(orders_balances_ids - all_balances_ids)[0: 3]}..."
    )

    print(
        f"in orders but mt-ticker is missing "
        f"{len(mtless_balances_ids.intersection(orders_balances_ids))}, "
        f"{list(mtless_balances_ids.intersection(orders_balances_ids))[0:3]}"
    )
    print(
        f"inconsistent balances to cancel "
        f"{len(mtless_balances_ids - orders_balances_ids)}, "
        f"{list(map(int, list(mtless_balances_ids - orders_balances_ids)))}\n"
    )

    to_cancel = {}
    for balance_id in list(mtless_balances_ids - orders_balances_ids):
        to_cancel[balance_id] = balances[balance_id]
        # print(to_cancel[balance_id]['id'])

    to_cancel_deps = 0
    to_cancel_withs = 0

    for balance in to_cancel.values():
        if balance["type"] == 'Deposit':
           to_cancel_deps += float(balance["Amount"])
        if balance["type"] == 'Withdrawal':
            to_cancel_withs += float(balance["Amount"])

    print(
        f"Deposits: {to_cancel_deps}\n"
        f"Withdrawals: {to_cancel_withs}"
    )
