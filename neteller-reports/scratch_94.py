import csv
import json
from collections import defaultdict

from exness.models import Balance

CSV_FILENAMES = [
    "NETELLER_Transfer_History_20200401_to_20200430 copy-mod.csv",
    "NETELLER_Transfer_History_20200501_to_20200531 copy-mod.csv",
    "NETELLER_Transfer_History_20200601_to_20200630 copy-mod.csv",
    "NETELLER_Transfer_History_20200701_to_20200801 copy-mod.csv",
]


def update_transaction(exness_id, neteller_id):
    try:
        balance = Balance.objects.get(id=exness_id)
    except Balance.DoesNotExist:
        return "Balance {} doesnt exist".format(exness_id)

    if balance.TransactionID != neteller_id:
        balance.TransactionID = neteller_id
        balance.save()


def parse_report(filename):
    rows = []

    with open(filename, "r") as report_csv:
        report_reader = csv.reader(report_csv, delimiter=',')
        for num, charge in enumerate(report_reader):
            if num == 0:
                header = charge
                continue

            rows.append({"neteller_id": charge[2], "exness_id": charge[3]})

    return header, rows


def process_report(filename):
    headers, report = parse_report(filename=filename)
    results = defaultdict(int)

    for num, row in enumerate(report):
        print("Processing {} of {} records".format(num, len(report)))
        exness_id = row["exness_id"]
        neteller_id = row["neteller_id"]
        results[exness_id] = update_transaction(exness_id=exness_id, neteller_id=neteller_id)

    print("Flushing update report")
    with open("update-report.json", "w") as update_json:
        update_json.writelines(json.dumps(results))


if __name__ == "__main__":
    for filename in CSV_FILENAMES:
        print("Processing {}".format(filename))
        process_report(filename=filename)

    print("Done")
