import matplotlib.pyplot as plt

# at THURSDAY, 16:00

data = {
    "5 DEC": {"rpm": 51, "app_server": 2.400, "error_rate": 0.33},
    "12 DEC": {"rpm": 42.2, "app_server": 2.260, "error_rate": 0.0942},
    "19 DEC": {"rpm": 45.5, "app_server": 2.500, "error_rate": 0.17},
    "26 DEC": {"rpm": 31.5, "app_server": 2.230, "error_rate": 0.26},
    "2 JAN": {"rpm": 33.2, "app_server": 2.320, "error_rate": 0.34},
    "9 JAN": {"rpm": 62.3, "app_server": 2.540, "error_rate": 0.24},
    "16 JAN": {"rpm": 45.4, "app_server": 2.280, "error_rate": 0.30},
    "23 JAN": {"rpm": 44.2, "app_server": 2.370, "error_rate": 0.25},
    "30 JAN": {"rpm": 46.8, "app_server": 2.120, "error_rate": 0.29},
    "6 FEB": {"rpm": 58.1, "app_server": 2.320, "error_rate": 0.26},
    "13 FEB": {"rpm": 48.7, "app_server": 2.500, "error_rate": 0.31},
    "20 FEB": {"rpm": 57.9, "app_server": 2.750, "error_rate": 0.36},
    "27 FEB": {"rpm": 67.2, "app_server": 2.81, "error_rate": 0.66},
    "5 MAR": {"rpm": 71.8, "app_server": 2.830, "error_rate": 0.22},
    "12 MAR": {"rpm": 81.7, "app_server": 3.150, "error_rate": 0.12},
    "19 MAR": {"rpm": 112, "app_server": 3.220, "error_rate": 0.36},
    "26 MAR": {"rpm": 93.5, "app_server": 2.980, "error_rate": 0.42},
    "2 APR": {"rpm": 86.4, "app_server": 2.490, "error_rate": 0.28},
    "9 APR": {"rpm": 80.9, "app_server": 2.550, "error_rate": 0.29},
    "16 APR": {"rpm": 80, "app_server": 2.470, "error_rate": 0.28},
    "23 APR": {"rpm": 90.3, "app_server": 2.390, "error_rate": 0.30},
    "30 APR": {"rpm": 74.7, "app_server": 2.400, "error_rate": 0.43},
    "7 MAY": {"rpm": 73.8, "app_server": 2.280, "error_rate": 0.43},
    "14 MAY": {"rpm": 75.2, "app_server": 2.280, "error_rate": 0.39},
    "21 MAY": {"rpm": 77.1, "app_server": 2.270, "error_rate": 0.39},
    "28 MAY": {"rpm": 76.4, "app_server": 2.370, "error_rate": 0.41},
    "4 JUN": {"rpm": 90.4, "app_server": 2.330, "error_rate": 0.47},
    "11 JUN": {"rpm": 83.4, "app_server": 2.530, "error_rate": 0.53},
    "18 JUN": {"rpm": 82.2, "app_server": 2.610, "error_rate": 0.67},
    "25 JUN": {"rpm": 80.8, "app_server": 2.750, "error_rate": 0.61},
    "25 JUN": {"rpm": 80.8, "app_server": 2.750, "error_rate": 0.61},
    "2 JUL": {"rpm": 86.7, "app_server": 2.600, "error_rate": 0.66},
    "9 JUL": {"rpm": 85, "app_server": 2.620, "error_rate": 0.65},
    "16 JUL": {"rpm": 83.5, "app_server": 2.330, "error_rate": 0.58},
}


def build_load_graph(data):
    fig = plt.figure()
    ax = fig.add_subplot()

    ax.plot(
        [row for row in data], [data[row]["rpm"] for row in data],
    )

    for row in data:
        ax.annotate(
            f"{data[row]['app_server']}",
            xy=(row, data[row]["rpm"]),
            xytext=(row, data[row]["rpm"] + 1),
        )

    plt.show()


def build_error_rate_graph(data):
    fig = plt.figure()
    ax = fig.add_subplot()

    ax.plot(
        [row for row in data], [data[row]["error_rate"] for row in data],
    )

    for row in data:
        ax.annotate(
            f"{data[row]['rpm']}",
            xy=(row, data[row]["error_rate"]),
            xytext=(row, data[row]["error_rate"] + 0.01),
        )

    plt.show()


build_load_graph(data)
