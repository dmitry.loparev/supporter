import csv
from datetime import datetime
from dateutil import parser
from pydantic import BaseModel

NETELLER_FILENAME = "NETELLER_Transfer_History_20200515_to_20200611.csv"
BALANCES_FILENAME = "neteller_balances_with_ids.csv"

FROM_DT = datetime.fromisoformat("2020-05-20 00:00:56")
UNTIL_DT = datetime.fromisoformat("2020-06-11 07:24:07")


PROCESSING = 0
ACCEPTED = 1
REJECTED = -1
CANCELED = -2
INT_PROCESSED = 3
PENDING = 2


def is_in_time(datetime_iso):
    return FROM_DT < parser.parse(datetime_iso) < UNTIL_DT


class NetellerTxRecord(BaseModel):
    date: str
    tx_type: str
    subtype: str
    neteller_tx_id: str
    merchant_ref_id: str
    description: str
    member_email: str
    account_id: str
    first_name: str
    last_name: str
    amount: str
    fee: str
    total_amount: str
    currency: str
    status: str
    status_reason: str

    class Config:
        fields = {
            "date": "Date",
            "tx_type": "Type",
            "subtype": "Subtype",
            "neteller_tx_id": "NETELLER Trans. ID",
            "merchant_ref_id": "Merchant Ref. ID",
            "description": "Description",
            "member_email": "Member Email",
            "account_id": "Account ID",
            "first_name": "First Name",
            "last_name": "Last Name",
            "amount": "Amount",
            "fee": "Fee",
            "total_amount": "Total Amount",
            "currency": "Currency",
            "status": "Status",
            "status_reason": "Status Reason"
        }


class BalanceTxRecord(BaseModel):
    invoice_id: str
    amount: str
    approve_datetime: str
    commission: str
    datetime: str
    eps_comission: str
    in_currency_amount: str
    int_main_id: str
    is_int: str
    is_return: str
    locked: str
    login: str
    mt_ticker: str
    multi_login_id: str
    noauto_reason: str
    operation_currency: str
    operation_type: str
    refund_deposit_id: str
    refunded: str
    staff_id: str
    status: int
    target_transaction_id: str
    transaction_id: str
    tx_type: str
    usd_amount: str
    usd_commission: str
    usd_deps_commission: str

    class Config:
        fields = {
            "invoice_id": "Invoice ID",
            "amount": "Amount",
            "approve_datetime": "Approve Datetime",
            "commission": "Commission",
            "datetime": "Datetime",
            "eps_comission": "Epscommission",
            "in_currency_amount": "Incurrencyamount",
            "int_main_id": "IntMainID",
            "is_int": "Isint",
            "is_return": "Is Return",
            "locked": "Locked",
            "login": "Login",
            "mt_ticker": "Mt Ticker",
            "multi_login_id": "Multi Login ID",
            "noauto_reason": "Noauto Reason",
            "operation_currency": "Operationcurrency",
            "operation_type": "Operationtype",
            "refund_deposit_id": "Refund Deposit ID",
            "refunded": "Refunded",
            "staff_id": "Staff ID",
            "status": "Status",
            "target_transaction_id": "Target Transaction ID",
            "transaction_id": "Transaction ID",
            "tx_type": "Type",
            "usd_amount": "Usdamount",
            "usd_commission": "Usdcommission",
            "usd_deps_commission": "Usdepscommission",
        }


def get_neteller_deposits():
    with open(NETELLER_FILENAME) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_number = 0
        header = []
        transactions = []

        for line in csv_reader:
            if line_number == 0:
                header = line
                line_number += 1
                continue

            neteller_tx = NetellerTxRecord(**dict(zip(header, line)))

            if neteller_tx.tx_type == 'Incoming' and is_in_time(neteller_tx.date):
                transactions.append(neteller_tx)

            line_number += 1

        return transactions


def get_billing_deposits():
    with open(BALANCES_FILENAME) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_number = 0
        header = []
        transactions = []
        for line in csv_reader:
            if line_number == 0:
                header = line
                line_number += 1
                continue

            billing_tx = BalanceTxRecord(**dict(zip(header, line)))

            if billing_tx.is_int == 'false' and billing_tx.status == ACCEPTED and billing_tx.tx_type == 'Deposit' and is_in_time(billing_tx.datetime):
                transactions.append(billing_tx)

            line_number += 1

        return transactions


# if __name__ == "__main__":
neteller_txs = get_neteller_deposits()
billing_txs = get_billing_deposits()
print(len(neteller_txs))
print(len(billing_txs))

neteller_merchant_ids = set([neteller_tx.merchant_ref_id for neteller_tx in neteller_txs])
billing_ids = set([billing_tx.invoice_id for billing_tx in billing_txs])

print(billing_ids - neteller_merchant_ids)
