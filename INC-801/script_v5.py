import csv
from exness.models import Balance
from exness.models_share import Logins

from exness.Secured.API.mt_billing_api import change_balance

from exness.Secured.Convert import CConvert


FILENAME = "Invoices.csv"

failed_withdrawals = [
    128490850,
    128491743,
    128493959,
    128494225,
    128494488,
    128494531,
    128494629,
    128495698,
    128496448,
    128497028,
    128497994,
    128498399,
    128498422,
    128498664,
    128498725,
    128489764,
    128491217,
    128492323,
    128494796,
    128495267,
    128497205,
    128497471,
    128499152,
    128499507,
]


def create_charge_by_withdrawal(failed_withdrawal, converter):
    login = Logins.objects.get(Login=failed_withdrawal.Account)

    params = {
        "Type": "Deposit",
        "Status": 1,
        "Server": login.Server,
        "OperationType": failed_withdrawal.OperationType,
        "Login": failed_withdrawal.Account,
        "Account": failed_withdrawal.Login,
        "TransactionID": failed_withdrawal.id,

        "EPSCommission": 0,
        # kind of trace for future generations
        "BenAddress": "Withdrawal id={} (INC-801)".format(failed_withdrawal.id),

        "Amount": converter.Do(
            failed_withdrawal.OperationCurrency,
            login.currency,
            failed_withdrawal.InCurrencyAmount
        ),
        "UserCurrency": login.currency,

        "CInCurrencyAmount": converter.Do(
            failed_withdrawal.OperationCurrency,
            login.currency,
            failed_withdrawal.CInCurrencyAmount,
        ),

        "CAmount": converter.Do(
            failed_withdrawal.OperationCurrency,
            login.currency,
            failed_withdrawal.CAmount,
        ),

        "InCurrencyAmount": converter.Do(
            failed_withdrawal.OperationCurrency,
            login.currency,
            failed_withdrawal.InCurrencyAmount,
        ),
        "OperationCurrency": login.currency,
    }

    balance = Balance.objects.create(**params)
    return balance


def process():
    balance_ids = failed_withdrawals
    converter = CConvert()

    for num, balance_id in enumerate(balance_ids):
        print("\n({}/{}) processing invoice {}".format(num+1, len(balance_ids), balance_id))

        try:
            failed_withdrawal = Balance.objects.get(id=balance_id)
            print("found withdrawal {}".format(balance_id))
        except Exception as e:
            print("cannot fetch FAILED BALANCE {}".format(balance_id))
            print(e)
            continue

        try:
            fair_charge = create_charge_by_withdrawal(failed_withdrawal, converter)
            print("created fair charge {} with id {}".format(fair_charge, fair_charge.id))
        except Exception as e:
            print("failed while creating fair charge for FAILED BALANCE {}".format(balance_id))
            print(e)
            continue

        try:
            order_id = change_balance(fair_charge)
            fair_charge.MT_Ticker = order_id
            fair_charge.save()
            print("successfully processed FAILED BALANCE {}, FAIR CHARGE ID = ".format(balance_id, fair_charge.id))

        except Exception as e:
            print("failed while charging FAILED BALANCE {}".format(balance_id))
            print(e)
            continue
