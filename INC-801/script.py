# import csv
# from exness.models import Balance
# from exness.models_share import Logins
#
# from exness.Secured.API.mt_billing_api import change_balance
#
#
# FILENAME = "Invoices.csv"
#
#
# def create_charge_by_withdrawal(failed_withdrawal):
#     login = Logins.objects.get(Login=failed_withdrawal.Account)
#
#     balance = Balance.objects.create(
#         Type="Deposit",
#         Status=1,
#         Server=login.Server,
#         OperationType=failed_withdrawal.OperationType,
#         Login=failed_withdrawal.Account,
#         Account=failed_withdrawal.Login,
#         TransactionID=failed_withdrawal.id,
#         # DateTime
#         OperationCurrency=failed_withdrawal.OperationCurrency,
#         InCurrencyAmount=failed_withdrawal.InCurrencyAmount,
#         CInCurrencyAmount=failed_withdrawal.CInCurrencyAmount,
#         UserCurrency=failed_withdrawal.UserCurrency,
#         Amount=failed_withdrawal.Amount,
#         CAmount=failed_withdrawal.CAmount,
#         # kind of trace for future generations
#         BenAddress="Withdrawal id={} (INC-801)".format(failed_withdrawal.id)
#     )
#
#     return balance
#
#
# def process():
#     invoices = []
#
#     with open(FILENAME, "r") as invoices_csv:
#         reader = csv.reader(invoices_csv, delimiter=',')
#         for charge in reader:
#             invoices.append(charge)
#
#     invoices = sorted(invoices, key=lambda invoice: invoice[0])
#
#     for num, invoice in enumerate(invoices):
#         print("\n({}/{}) processing invoice {}".format(num+1, len(invoices), invoice[0]))
#         balance_id, login_from, optype, amount_currency, login_to, balance_date, balance_status, _, _ = invoice
#
#         try:
#             failed_withdrawal = Balance.objects.get(id=balance_id)
#         except Exception as e:
#             print("cannot fetch FAILED BALANCE {}".format(balance_id))
#             print(e)
#             continue
#
#         try:
#             fair_charge = create_charge_by_withdrawal(failed_withdrawal)
#         except Exception as e:
#             print("failed while creating fair charge for FAILED BALANCE {}".format(balance_id))
#             print(e)
#             continue
#
#         try:
#             order_id = change_balance(fair_charge)
#             fair_charge.MT_Ticker = order_id
#             fair_charge.save()
#             print("successfully processed FAILED BALANCE {}, FAIR CHARGE ID = ".format(balance_id, fair_charge.id))
#
#         except Exception as e:
#             print("failed while charging FAILED BALANCE {}".format(balance_id))
#             print(e)
#             continue
#
#
# if __name__ == "__main__":
#     process()
